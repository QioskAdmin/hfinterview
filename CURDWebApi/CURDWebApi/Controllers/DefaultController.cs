﻿using CURDWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CURDWebApi.Controllers
{
    public class DefaultController : ApiController
    {
        private SampleDBEntities db = new SampleDBEntities();

        [HttpPost]
        [Route("Default/Create")]
        public IHttpActionResult Create(Categories model)
        {
            db.Categories.Add(model);
            db.SaveChanges();

            List<Categories> myCategories = db.Categories.ToList();
            return Ok(myCategories);
        }

        [HttpPost]
        [Route("Default/Update")]
        public IHttpActionResult Update(Categories model)
        {
            Categories updateCategory = db.Categories.Where(x => x.CategoryID == model.CategoryID).FirstOrDefault();

            if (updateCategory == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                updateCategory.CategoryName = model.CategoryName;
                updateCategory.Description = model.Description;
                db.SaveChanges();

                List<Categories> myCategories = db.Categories.ToList();
                return Ok(myCategories);
            }
        }

        [HttpGet]
        [Route("Default/Read")]
        public IHttpActionResult Read()
        {
            List<Categories> myCategories = db.Categories.ToList();

            if (myCategories == null)
                return StatusCode(HttpStatusCode.NoContent);
            else
                return Ok(myCategories);
        }

        [HttpGet]
        [Route("Default/Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            Categories deleteCategory = db.Categories.Where(x => x.CategoryID == id).FirstOrDefault();

            if (deleteCategory == null)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                db.Categories.Remove(deleteCategory);
                db.SaveChanges();

                List<Categories> myCategories = db.Categories.ToList();
                return Ok(myCategories);
            }

        }
    }
}
